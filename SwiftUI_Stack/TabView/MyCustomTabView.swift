//
//  MyCustomTabView.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 31/12/2022.
//

import SwiftUI

enum TabIndex {
    case home
    case cart
    case profile
}

struct MyCustomTabView: View {
    
    @State var tabIndex: TabIndex = .home
    
    @State var largerScale: CGFloat = 1.4
    
    func changeMyView(tabIndex: TabIndex) -> MyView {
        switch tabIndex {
        case .home:
            return MyView(title: "Home", bgColor: .blue)
        case .cart:
            return MyView(title: "Cart", bgColor: .yellow)
        case .profile:
            return MyView(title: "Profile", bgColor: .purple)
        }
    }
    
    func changeIconColor(tabIndex: TabIndex) -> Color {
        switch tabIndex {
        case .home:
            return Color.blue
        case .cart:
            return Color.yellow
        case .profile:
            return Color.purple
        }
    }
    
    func calcCircleBgPosition(tabIndex: TabIndex, _ geometry: GeometryProxy) -> CGFloat {
        switch tabIndex {
        case .home:
            return -(geometry.size.width / 3)
        case .cart:
            return 0
        case .profile:
            return geometry.size.width / 3
        }
    }
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .bottom) {
                self.changeMyView(tabIndex: self.tabIndex)
                
                // Tab Bar
                Circle()
                    .foregroundColor(.white)
                    .frame(width: 90, height: 90)
                    .offset(x: self.calcCircleBgPosition(tabIndex: self.tabIndex, geometry), y: UIApplication.shared.windows.first?.safeAreaInsets.bottom == 0 ? 20 : 0)
                
                VStack(spacing: 0) {
                    HStack(spacing: 0) {
                        Button(action:{
                            print("Home Tapped")
                            withAnimation{
                                self.tabIndex = .home
                            }
                        }){
                            Image(systemName: "house.fill")
                                .font(.system(size: 25))
                                .scaleEffect(self.tabIndex == .home ? self.largerScale : 1.0)
                                .foregroundColor(self.tabIndex == .home ? self.changeIconColor(tabIndex: tabIndex) : Color.gray)
                                .frame(width: geometry.size.width / 3, height: 50)
                                .offset(y: self.tabIndex == .home ? -10 : 0)
                        }.background(.white)
                        
                        Button(action:{
                            print("Cart Tapped")
                            withAnimation {
                                self.tabIndex = .cart
                            }
                        }){
                            Image(systemName: "cart.fill")
                                .font(.system(size: 25))
                                .scaleEffect(self.tabIndex == .cart ? self.largerScale : 1.0)
                                .foregroundColor(self.tabIndex == .cart ? self.changeIconColor(tabIndex: tabIndex) : Color.gray)
                                .frame(width: geometry.size.width / 3, height: 50)
                                .offset(y: self.tabIndex == .cart ? -10 : 0)
                        }.background(.white)
                        
                        Button(action:{
                            print("Profile Tapped")
                            withAnimation {
                                self.tabIndex = .profile
                            }
                        }){
                            Image(systemName: "person.circle.fill")
                                .font(.system(size: 25))
                                .scaleEffect(self.tabIndex == .profile ? self.largerScale : 1.0)
                                .foregroundColor(self.tabIndex == .profile ? self.changeIconColor(tabIndex: tabIndex) : Color.gray)
                                .frame(width: geometry.size.width / 3, height: 50)
                                .offset(y: self.tabIndex == .profile ? -10 : 0)
                        }.background(.white)
                    }
                    Rectangle() // 탭뷰 bottom 간격 설정
                    // iphoneSE 경우 safeAreaInsets.bottom이 0임
                        .frame(height: UIApplication.shared.windows.first?.safeAreaInsets.bottom == 0 ? 0 : 20)
                        .foregroundColor(.white)
                }
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct MyCustomTabView_Previews: PreviewProvider {
    static var previews: some View {
        MyCustomTabView(tabIndex: .home)
    }
}
