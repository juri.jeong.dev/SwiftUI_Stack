//
//  MyList.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 25/12/2022.
//

import SwiftUI

struct MyList: View {
    
    @Binding var isNavigationBarHidden: Bool
    
    // Section header 색 없애기
    init(isNavigationBarHidden: Binding<Bool> = .constant(false)) {
        if #available(iOS 14.0, *) {

        } else {
            UITableView.appearance().tableFooterView = UIView()
        }

        UITableView.appearance().separatorStyle = .none
        _isNavigationBarHidden = isNavigationBarHidden // 외부에서 넘어온 값으로 설정해준다는 의미
    }
    
    var body: some View {
//        List {
//            ForEach(1...10, id: \.self) {
//                Text("This is List \($0)")
//            }
//        }
        List() {
            
            Section(
                header:
                    Text("This is Header")
                    .font(.headline)
                    .foregroundColor(.blue),
                footer: Text("This is Footer")
            ){
                ForEach(1...4, id: \.self) { itemIndex in
    //                Text("This is List \(itemIndex)")
                    MyCard(icon: "doc.fill", title: "Preview \(itemIndex)", start: "1 PM", end: "3 PM", bgColor: .pink)
                }
            }
            .listRowInsets(EdgeInsets.init(top: 10, leading: 10, bottom: 10, trailing: 10))
            
            Section(
                header: Text("This is Second Header"),
                footer: Text("This is Secon Footer")
                    .font(.caption)
            ){
                ForEach(1...4, id: \.self) { itemIndex in
                    MyCard(icon: "doc.fill", title: "Preview \(itemIndex)", start: "1 PM", end: "3 PM", bgColor: .blue)
                }
            }
            .listRowInsets(EdgeInsets.init(top: 10, leading: 50, bottom: 10, trailing: 10))
            .listRowBackground(Color.yellow) // 리스트 배경색
        }
//            .listStyle(InsetGroupedListStyle())
//        .listStyle(InsetListStyle())
//        .listStyle(SidebarListStyle())
//        .listStyle(PlainListStyle())
        .listStyle(GroupedListStyle())
        .navigationTitle("My List") // 이 뷰로 넘어왔을 때 보여지게 됨
        .onAppear {
            self.isNavigationBarHidden = false
        }
    }
}

struct MyList_Previews: PreviewProvider {
    static var previews: some View {
        MyList()
    }
}
