//
//  MyVStack.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 25/12/2022.
//

import SwiftUI

struct MyVStack: View {
    var body: some View {
        VStack(alignment: .trailing, spacing: 20) {
            // VStack 전체를 오른쪽으로 치우치게 하는 방법
            // 1. 디바이더 사용
//            Divider()
//                .opacity(0) // 나눔선 안보이게 설정 
            // 2. Rectangle 사용
//            Rectangle()
//                .frame(height: 1)
            
            Text("Hello, world!")
                .font(.system(size: 30))
                .fontWeight(.heavy)

            Rectangle() // 기본적으로 frame 적용 됨
                .frame(width: 100, height: 100)
                .foregroundColor(Color.red)
            Rectangle()
                .frame(width: 100, height: 100)
                .foregroundColor(Color.yellow)
            
            // Spacer으로 frame 조절 가능
            Spacer()
                .frame(height: 40)
            
            Rectangle()
                .frame(width: 100, height: 100)
                .foregroundColor(Color.blue)
                
        }
        .frame(width: 300)
        .padding(.vertical, 30)
        .background(Color.white)
        
        //.edgesIgnoringSafeArea(.bottom)
    }
}

struct MyVStack_Previews: PreviewProvider {
    static var previews: some View {
        MyVStack()
    }
}

