//
//  SwiftUI_StackApp.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 25/12/2022.
//

import SwiftUI

@main
struct SwiftUI_StackApp: App {
    var body: some Scene {
        WindowGroup {
            MyCustomTabView()
        }
    }
}
