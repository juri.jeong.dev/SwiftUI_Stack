//
//  ContentView.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 25/12/2022.
//

import SwiftUI

struct ContentView: View {
    
    @State var isNavigationBarHidden: Bool = false
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .bottomTrailing) {
                VStack(alignment: .leading, spacing: 0) {
                    
                    HStack {
                        
                        NavigationLink(destination: MyList(isNavigationBarHidden: self.$isNavigationBarHidden)) {
                            Image(systemName: "line.horizontal.3")
                                .font(.largeTitle)
                                .foregroundColor(.black)
                        }
                        
                        Spacer()
                        
                        NavigationLink(destination: MyProfile(isNavigationBarHidden: self.$isNavigationBarHidden)) {
                            Image(systemName: "person.crop.circle.fill")
                                .font(.largeTitle)
                                .foregroundColor(.black)
                        }
                    }
                    .padding(20)
                    
                    Spacer().frame(height: 20)
                    
                    Text("Jenny To do list")
                        .font(.system(size: 40))
                        .fontWeight(.black)
                        .padding(.horizontal, 20)
                    
                    ScrollView {
                        VStack {
                            MyProjectCard()
                            MyBasicCard()
                            MyCard(icon: "tray.fill", title: "Clean up the table", start: "3 PM", end: "4 PM", bgColor: .secondary)
                            MyCard(icon: "tray.fill", title: "Clean up the table", start: "3 PM", end: "4 PM", bgColor: .secondary)
                            MyCard(icon: "tray.fill", title: "Clean up the table", start: "3 PM", end: "4 PM", bgColor: .secondary)
                            MyCard(icon: "tray.fill", title: "Clean up the table", start: "3 PM", end: "4 PM", bgColor: .secondary)
                        }
                        .padding(.top, 20)
                        .padding(.horizontal, 20)
                    }
                }
                
                Circle()
                    .foregroundColor(.gray)
                    .frame(width: 60, height: 60)
                    .overlay(
                        Image(systemName: "plus")
                            .font(.system(size: 30))
                            .foregroundColor(.white)
                    )
                    .padding(.trailing, 10)
                    .shadow(radius: 20)
            }
            .navigationTitle("Main")
            .navigationBarHidden(self.isNavigationBarHidden)
            // 네비게이션바가 보여지게 될 때
            .onAppear{
                self.isNavigationBarHidden = true
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
