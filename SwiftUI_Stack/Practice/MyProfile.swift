//
//  MyProfile.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 26/12/2022.
//

import SwiftUI

struct MyProfile: View {
    
    @Binding var isNavigationBarHidden: Bool
    
    init(isNavigationBarHidden: Binding<Bool> = .constant(false)) {
        _isNavigationBarHidden = isNavigationBarHidden
    }
    
    var body: some View {
        // 외부에서 navigationView로 감싸져왔기 때문에 여기서는 설정해주지 않아도 됨
        ScrollView {
            VStack() {
                MyCircleImageView(imageString: "4")
                    .padding(.vertical, 20)
                
                Text("Devleoper Jenny")
                    .font(.system(size: 30))
                    .fontWeight(.black)
                
                Spacer().frame(height: 20)
                
                Text("Subscribe Me :)")
                    .font(.system(size: 24))
                    .fontWeight(.bold)
                
                HStack {
                    Text("Go Subscribe")
                        .font(.system(size: 20))
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .padding()
                        .background(.red)
                        .cornerRadius(20)
                    Text("Go Open Chat")
                        .font(.system(size: 20))
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .padding()
                        .background(.yellow)
                        .cornerRadius(20)
                }
                Spacer()
            }
        }
        .navigationTitle("My Profile")
        .onAppear{
            self.isNavigationBarHidden = false
        }
        .navigationBarItems(trailing:
            NavigationLink(destination:
                Text("설정 화면")
                .font(.largeTitle)
                .fontWeight(.black)
              ){
                Image(systemName: "gear")
                    .foregroundColor(.black)
            })
    }
}

struct MyProfile_Previews: PreviewProvider {
    static var previews: some View {
        MyProfile()
    }
}
