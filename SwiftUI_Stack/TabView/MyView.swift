//
//  MyView.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 31/12/2022.
//

import SwiftUI

struct MyView: View {
    
    // MyTabView의 배경
    var title: String
    var bgColor: Color
    
    var body: some View {
        ZStack {
            bgColor
                .edgesIgnoringSafeArea(.top)
            Text(title)
                .font(.largeTitle)
                .fontWeight(.black)
                .foregroundColor(.white)
        }
        .animation(.none)
    }
}

struct MyView_Previews: PreviewProvider {
    static var previews: some View {
        MyView(title: "1번", bgColor: .pink)
    }
}
