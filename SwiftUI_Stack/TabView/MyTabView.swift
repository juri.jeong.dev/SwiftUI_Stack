//
//  MyTabView.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 31/12/2022.
//

import SwiftUI

struct MyTabView: View {
    
    var body: some View {
        TabView {
            MyView(title: "1번", bgColor: .pink)
                .fontWeight(.black)
                .font(.largeTitle)
                .tabItem{
                    Image(systemName: "airplane")
                    Text("1")
                }
                .tag(0)
            
            MyView(title: "2번", bgColor: .purple)
                .fontWeight(.black)
                .font(.largeTitle)
                .foregroundColor(.purple)
                .tabItem{
                    Image(systemName: "flame.fill")
                    Text("2")
                }
                .tag(1)
            
            MyView(title: "3번", bgColor: .black)
                .fontWeight(.black)
                .font(.largeTitle)
                .foregroundColor(.pink)
                .tabItem{
                    Image(systemName: "doc.fill")
                    Text("3")
                }
                .tag(2)
        }
    }
}

struct MyTabView_Previews: PreviewProvider {
    static var previews: some View {
        MyTabView()
    }
}
