//
//  MyBasicCard.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 25/12/2022.
//

import SwiftUI

struct MyBasicCard: View {
    
    var body: some View {
    
        HStack(spacing: 20) {
            
            Image(systemName: "flame.fill")
                .font(.system(size: 40))
                .foregroundColor(.white)
            
            VStack(alignment: .leading, spacing: 0) {
                Divider().opacity(0)
                
                Text("Youtube live Burning")
                    .fontWeight(.bold)
                    .font(.system(size: 23))
                    .foregroundColor(.white)
                
                Spacer().frame(height: 8)
                
                Text("8 PM - 10 PM")
                    .foregroundColor(.white)
            }
        }
        .padding(30)
        .background(.purple)
        .cornerRadius(20)
    }
}

struct MyBasicCard_Previews: PreviewProvider {
    static var previews: some View {
        MyBasicCard()
    }
}

