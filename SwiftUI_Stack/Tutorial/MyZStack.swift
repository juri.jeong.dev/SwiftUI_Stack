//
//  MyZStack.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 25/12/2022.
//

import SwiftUI

struct MyZStack: View {
    var body: some View {
        ZStack {
            Rectangle()
                .frame(width: 300, height: 300)
                .foregroundColor(.red)
                .zIndex(0) // zIndex 기본은 0
            
            Rectangle()
                .frame(width: 200, height: 200)
                .foregroundColor(.yellow)
                .offset(y: 100)
                .zIndex(1)
            
            Rectangle()
                .frame(width: 100, height: 100)
                .foregroundColor(.blue)
                .offset(y: 200)
                .zIndex(1)
        }
        .padding(50)
        .background(.gray)
    }
}

struct MyZStack_previews: PreviewProvider {
    static var previews: some View {
        MyZStack()
    }
}
