//
//  MyHStack.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 25/12/2022.
//

import SwiftUI

struct MyHStack: View {
    var body: some View {
        
        HStack(alignment: .center) {
            
//            Divider()
            
//            Rectangle()
//                .frame(width: 100)
//                .foregroundColor(.red)
            
            Image(systemName: "flame.fill")
                .foregroundColor(.white)
                .font(.system(size: 30))
            
            Text("Like")
                .font(.system(size: 35))
                .foregroundColor(.white)
            
//            Rectangle()
//                .frame(width: 100, height: 100)
//                .foregroundColor(.yellow)
//
//            Rectangle()
//                .frame(width: 100, height: 100)
//                .foregroundColor(.blue)
        }
        .padding()
        .background(.gray)
        .cornerRadius(20)
        .shadow(radius: 10)
    }
}

struct MyHStakc_Previews: PreviewProvider {
    static var previews: some View {
        MyHStack()
    }
}
