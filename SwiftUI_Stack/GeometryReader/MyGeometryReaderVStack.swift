//
//  MyGeometryReaderVStack.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 31/12/2022.
//

import SwiftUI

enum Index {
    case one, two, three
}

struct MyGeometryReaderVStack: View {

    @State var index: Index = .one
    
    let centerPosition: (GeometryProxy) -> CGPoint = { proxy in
        return CGPoint(x: proxy.frame(in: .local).midX,
                       y: proxy.frame(in: .local).midY)
    }
    
    var body: some View {
        GeometryReader { proxy in
            VStack(spacing: 0) {
                Button(action: {
                    print("1번이 클릭됨")
                    withAnimation {
                        self.index = .one
                    }
                }){
                    Text("1")
                        .font(.largeTitle)
                        .fontWeight(.black )
                        .frame(width: 100, height: proxy.size.height / 3)
                        .padding(.horizontal, self.index == .one ? 50 : 0)
                        .background(.purple)
                }
                
                Button(action: {
                    print("2번이 클릭됨")
                    withAnimation {
                        self.index = .two
                    }
                }){
                    Text("2")
                        .font(.largeTitle)
                        .fontWeight(.black )
                        .frame(width: 100, height: proxy.size.height / 3)
                        .padding(.horizontal, self.index == .two ? 50 : 0)
                        .background(.yellow)
                }
                
                Button(action: {
                    print("3번이 클릭됨")
                    withAnimation {
                        self.index = .three
                    }
                }){
                    Text("3")
                        .font(.largeTitle)
                        .fontWeight(.black )
                        .frame(width: 100, height: proxy.size.height / 3)
                        .padding(.horizontal, self.index == .three ? 50 : 0)
                        .background(.red)
                }
            }
            .position(centerPosition(proxy))
        }
        .background(.gray)
    }
}

struct MyGeometryReaderVStack_Previews: PreviewProvider {
    static var previews: some View {
        MyGeometryReaderVStack()
    }
}
