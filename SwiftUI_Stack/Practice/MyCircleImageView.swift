//
//  MyCircleImageView.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 31/12/2022.
//

import SwiftUI

struct MyCircleImageView: View {
    
    var imageString: String
    
    var body: some View {
        Image(imageString)
            .resizable()
            .scaledToFit()
            .frame(width: 250, height: 250)
            .clipShape(Circle())
            .shadow(color: .gray, radius: 10, x: 0, y: 10)
            .overlay(
                // stroke Border: 안에 만들어짐
                Circle().strokeBorder(Color.yellow, lineWidth: 15)
            )
    }
}

struct MyCircleImageView_Previews: PreviewProvider {
    static var previews: some View {
        MyCircleImageView(imageString: "4")
    }
}
