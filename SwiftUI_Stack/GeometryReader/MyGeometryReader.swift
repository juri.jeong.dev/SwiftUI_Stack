//
//  MyGeometryReader.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 31/12/2022.
//

import SwiftUI

struct MyGeometryReader: View {
    var body: some View {
        GeometryReader { geometryReader  in
            HStack(spacing: 0) {
                Text("1")
                    .font(.largeTitle)
                    .fontWeight(.black)
                    .frame(width: geometryReader.size.width / 4)
                    .background(.red)
                
                Text("2")
                    .font(.largeTitle)
                    .fontWeight(.black)
                    .frame(width: geometryReader.size.width / 4)
                    .background(.blue)
                
                Text("3")
                    .font(.largeTitle)
                    .fontWeight(.black)
                    .frame(width: geometryReader.size.width / 4)
                    .background(.yellow)
                
                Text("4")
                    .font(.largeTitle)
                    .fontWeight(.black)
                    .frame(width: geometryReader.size.width / 4)
                    .background(.green)
            }
            .background(.white)
        }
        .background(.black)
    }
}

struct MyGeometryReader_Previews: PreviewProvider {
    static var previews: some View {
        MyGeometryReader()
    }
}
