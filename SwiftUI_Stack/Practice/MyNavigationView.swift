//
//  MyNavigationView.swift
//  SwiftUI_Stack
//
//  Created by Juri Jeong on 26/12/2022.
//

import SwiftUI

struct MyNavigationView: View {
    var body: some View {
        NavigationView {
//            Text("My NavigationView")
            MyList()
                .navigationBarTitle("Hello")
                .navigationBarItems(leading:
                                        Button(action: {
                                            print("Button leading Tapped")
                                        }){
                                            Image(systemName: "plus")
                                        },
                                        trailing:
                                        NavigationLink(destination: Text("넘어온 화면")) {
                                            Image(systemName: "bookmark.fill")
                                                .font(.system(size: 20))
                                                .foregroundColor(Color.black)
                                        }
                )
        }
    }
}

struct MyNavigationView_Previews: PreviewProvider {
    static var previews: some View {
        MyNavigationView()
    }
}
//  option + command + enter = preview 미리보기 창 띄움
